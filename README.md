# Container Scanning

Based on [Clair Scanner](https://github.com/arminc/clair-scanner) you can scan multiple Docker images for vulnerabilities.

## Usage

```bash
curl -sL https://gitlab.com/arivum/container-scanning/raw/master/scan | bash <image1:tag> [image2:tag...]
```

or

```bash
git clone https://gitlab.com/arivum/container-scanning
cd container-scanning
./scan <image1:tag> [image2:tag...]
```

## Environment variables

| **Variable** | **Description** | **Default** |
| -------- | ----------- | ------- |
| NO_CACHE | Disable Docker Images Cache if set to any value |  |