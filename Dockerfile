FROM docker:dind

RUN apk add --update curl wget jq bash
COPY bin/* /usr/bin/

VOLUME /export

ENTRYPOINT ["clair"]
